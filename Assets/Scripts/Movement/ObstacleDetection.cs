﻿using UnityEngine;

namespace Movement
{
    public class ObstacleDetection : MonoBehaviour
    {
        public bool CanMoveTowards(Vector3 vector, float lookAheadFactor)
        {
            var center = transform.position;
            center.y = 0.5f;
            return !Physics.Raycast(center, vector, vector.magnitude * lookAheadFactor);
        }
    }
}
