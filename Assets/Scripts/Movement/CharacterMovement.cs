﻿using Entities;
using UnityEngine;

namespace Movement
{
    public class CharacterMovement : MonoBehaviour
    {
        public Transform TargetTransform;
        public ObstacleDetection Detector;
        public Dash DashMove;

        public float LookAheadFactor;
        public float Speed;

        CharacterView view;
        Vector3 lastMoveVector;

        public void Initialize(CharacterView view)
        {
            this.view = view;
            DashMove.Initialize(TargetTransform, view, Detector);
        }

        public void MoveFromVector(Vector3 vector)
        {
            if (vector.magnitude > 0 && CanMove(vector))
            {
                lastMoveVector = vector;
                FaceMovement();
                TargetTransform.Translate(Vector3.forward * Speed);
            }

            view.AnimateRun(vector.magnitude * Speed);
        }

        bool CanMove(Vector3 vector) => !DashMove.enabled && Detector.CanMoveTowards(vector * Speed, LookAheadFactor);

        public void Dash() => DashMove.Do(lastMoveVector.normalized);

        void FaceMovement()
        {
            TargetTransform.LookAt(TargetTransform.position + lastMoveVector);
            NormalizeRotation();
        }

        void NormalizeRotation()
        {
            var normalizedEuler = new Vector3(0f, transform.eulerAngles.y, 0f);
            transform.eulerAngles = normalizedEuler;
        }
    }
}