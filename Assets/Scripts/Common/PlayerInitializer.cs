﻿using Entities;
using UnityEngine;

namespace Common
{
    public class PlayerInitializer : MonoBehaviour
    {
        public FollowAnchor CameraAnchor;
        public Character PlayerCharacter;

        void Start()
        {
            PlayerCharacter.Initialize();
            CameraAnchor.AnchorTo(PlayerCharacter.transform);
        }
    }
}
