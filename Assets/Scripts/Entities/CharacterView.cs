﻿using UnityEngine;

namespace Entities
{
    public class CharacterView : MonoBehaviour
    {
        public Animator AnimationController;
        public float RunSpeedFactor;
        
        public void AnimateRun(float speed) => AnimationController.SetFloat("MoveSpeed", speed * RunSpeedFactor);

        public void AnimateRoll(float rollSpeedFactor)
        {
            AnimationController.SetFloat("RollSpeedFactor", rollSpeedFactor);
            AnimationController.SetTrigger("Roll");
        }
    }
}
