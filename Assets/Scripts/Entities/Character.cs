﻿using Movement;
using UnityEngine;

namespace Entities
{
    public class Character : MonoBehaviour
    {
        public CharacterMovement Movement;
        public CharacterView View;

        public void Initialize()
        {
            Movement.Initialize(View);
        }
    }
}
